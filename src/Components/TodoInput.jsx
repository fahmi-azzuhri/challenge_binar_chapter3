import React from "react";

const TodoInput = ({
  updateData,
  changeTask,
  updateTask,
  cancelUpdate,
  newTask,
  setNewTask,
  addTask,
}) => {
  return (
    <>
      <h2 className="mt-5">Todo Input</h2>

      {updateData && updateData ? (
        <>
          {/*update task*/}
          <div className="row">
            <div className="col">
              <input
                value={updateData && updateData.task}
                onChange={(e) => changeTask(e)}
                className="form-control form-control-lg"
              />
            </div>
            <div className="col-auto">
              <button
                onClick={updateTask}
                className="btn btn-lg btn-primary mr-20"
              >
                Update
              </button>
              <button onClick={cancelUpdate} className="btn btn-lg btn-warning">
                Cancel
              </button>
            </div>
          </div>
          {/* end update task*/}
        </>
      ) : (
        <>
          {/*add task*/}
          <div className="row d-block">
            <div className="col d-flex">
              <input
                value={newTask}
                onChange={(e) => setNewTask(e.target.value)}
                className="form-control form-control-lg"
                placeholder="Add your task here"
              />
            </div>
            <div className="col-auto">
              <button
                onClick={addTask}
                className="btn btn-lg btn-primary w-100 mt-2"
              >
                Submit
              </button>
            </div>
          </div>
          {/*end add task*/}
        </>
      )}
    </>
  );
};

export default TodoInput;

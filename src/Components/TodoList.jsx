import React, { useState } from "react";
import TodoSearch from "./TodoSearch";
import "../App.css";
import TodoInput from "./TodoInput";
import TodoOutput from "./TodoOutput";

const TodoList = () => {
  const data = [
    {
      id: 1,
      task: "Nyuci mobil",
      complete: true,
    },
    {
      id: 2,
      task: "Memberi makan kucing",
      complete: true,
    },
    {
      id: 3,
      task: "Olahraga 10 menit",
      complete: false,
    },
    {
      id: 4,
      task: "Sarapan sereal",
      complete: true,
    },
    {
      id: 5,
      task: "Belanja harian",
      complete: false,
    },
    {
      id: 6,
      task: "Ngeprint tugas",
      complete: true,
    },
    {
      id: 7,
      task: "Bayar tagihan bulanan",
      complete: true,
    },
    {
      id: 8,
      task: "Berangkat kuliah",
      complete: false,
    },
    {
      id: 9,
      task: "Les bahasa Inggris",
      complete: true,
    },
    {
      id: 10,
      task: "Ke rumah Sabrina",
      complete: false,
    },
  ];
  const [todo, setTodo] = useState(data);
  const filterNames = (e) => {
    const search = e.target.value.toLowerCase();
    const filteredNames = data.filter((todo) =>
      todo.task.toLowerCase().includes(search)
    );
    setTodo(filteredNames);
  };

  //temp state
  const [newTask, setNewTask] = useState("");
  const [updateData, setUpdateData] = useState("");

  const addTask = () => {
    if (newTask) {
      let num = todo.length + 1;
      let newEntry = {
        id: num,
        task: newTask,
        complete: false,
      };
      setTodo([...todo, newEntry]);
      setNewTask("");
    }
  };

  const deleteTask = (id) => {
    let newTask = todo.filter((task) => task.id !== id);
    setTodo(newTask);
  };

  const markDone = (id) => {
    let newTask = todo.map((task) => {
      if (task.id === id) {
        return { ...task, complete: !task.complete };
      }
      return task;
    });
    setTodo(newTask);
  };

  const cancelUpdate = () => {
    setUpdateData("");
  };

  const changeTask = (e) => {
    let newEntry = {
      id: updateData.id,
      task: e.target.value,
      complete: updateData.complete ? true : false,
    };
    setUpdateData(newEntry);
  };

  const updateTask = () => {
    let filterRecords = [...todo].filter((task) => task.id !== updateData.id);
    let updatedObject = [...filterRecords, updateData];
    setTodo(updatedObject);
    setUpdateData("");
  };

  return (
    <div className="row">
      <div className="col-md-3"></div>
      <div className="col-md-6">
        <div className="TodoList">
          <TodoSearch todo={todo} filterNames={filterNames} />

          <TodoOutput
            todo={todo}
            markDone={markDone}
            setUpdateData={setUpdateData}
            deleteTask={deleteTask}
          />
          <TodoInput
            updateData={updateData}
            changeTask={changeTask}
            updateTask={updateTask}
            cancelUpdate={cancelUpdate}
            newTask={newTask}
            setNewTask={setNewTask}
            addTask={addTask}
          />
        </div>
      </div>
      <div className="col-md-3"></div>
    </div>
  );
};

export default TodoList;

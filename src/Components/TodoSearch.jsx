import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

const TodoSearch = ({ todo, filterNames }) => {
  return (
    <>
      <div className="row my-3">
        <h2>Todo Search</h2>
        <div className="col d-flex">
          <button className="btn btn-lg btn-primary mr-20">
            <FontAwesomeIcon icon={faMagnifyingGlass} />
          </button>
          <input
            className="form-control form-control-lg"
            onChange={(e) => filterNames(e)}
            placeholder="Search todo ..."
          />
        </div>

        <ul className="ulTodo">
          {todo.map((todos) => {
            return <li key={todos.id}>{todos.task}</li>;
          })}
        </ul>
      </div>
    </>
  );
};

export default TodoSearch;

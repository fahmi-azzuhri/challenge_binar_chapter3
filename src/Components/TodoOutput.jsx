/* eslint-disable no-undef */
import React from "react";
import {
  faSquareCheck,
  faPenToSquare,
  faTrashCan,
} from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const TodoOutput = ({ todo, markDone, setUpdateData, deleteTask }) => {
  return (
    <>
      {todo &&
        todo
          .sort((a, b) => (a.id > b.id ? 1 : -1))
          .map((task, index) => {
            return (
              <React.Fragment key={task.id}>
                <div className="col taskBg">
                  <div className={task.complete ? "done" : ""}>
                    <span className="taskNumber">{index + 1}</span>
                    <span className="taskText">{task.task}</span>
                  </div>
                  <div className="iconsWrap">
                    <span
                      title="Complete/No Completed"
                      onClick={(e) => markDone(task.id)}
                    >
                      <FontAwesomeIcon icon={faSquareCheck} />
                    </span>
                    {task.complete ? null : (
                      <span
                        title="Edit"
                        onClick={() =>
                          setUpdateData({
                            id: task.id,
                            task: task.task,
                            complete: task.complete ? true : false,
                          })
                        }
                      >
                        <FontAwesomeIcon icon={faPenToSquare} />
                      </span>
                    )}
                    <span title="Delete" onClick={() => deleteTask(task.id)}>
                      <FontAwesomeIcon icon={faTrashCan} />
                    </span>
                  </div>
                </div>
              </React.Fragment>
            );
          })}
      <div className="row d-flex">
        <div className="col text-center">
          <button className="btn btn-danger mx-5">Delete task done</button>
          <button className="btn btn-danger mx-5">Delete all task</button>
        </div>
      </div>

      <p className="TodoList">{todo && todo.length ? "" : "No Task.."}</p>
    </>
  );
};

export default TodoOutput;
